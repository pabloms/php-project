<?php

namespace App\Form;

use App\Entity\Dish;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DishFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Plato' => 'Plato',
                    'Postre' => 'Postre'
                ],
            ])
            ->add('origin', ChoiceType::class, [
                'choices' => [
                    'Italia' => 'Italia',
                    'España' => 'Espana',
                    'China' => 'China',
                    'Francia' => 'Francia',
                    'Japon' => 'Japon',
                    'Mexico' => 'Mexico',
                    'India' => 'India',
                    'Otros' => 'Otros'
                ]
            ])
            ->add('image', UrlType::class)
            ->add('vegan', ChoiceType::class, ['label'=>'Es Vegano? (Solo si es plato, no postre)',
                'choices' => [
                    'Si' => true,
                    'No' => false
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Dish::class,
        ]);
    }
}
