<?php

namespace App\Form;

use App\Entity\recipes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecipeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('difficulty', ChoiceType::class, [
                'choices' => [
                    'Principiante' => 'Principiante',
                    'Intermedio' => 'Intermedio',
                    'Experto' => 'Experto'
                ]
            ])
            ->add('ingredients', TextareaType::class)
            ->add('steps', TextareaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Recipes::class,
        ]);
    }
}
