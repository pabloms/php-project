<?php

namespace App\Controller;

use App\Entity\Dish;
use App\Form\DishFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;


class DishesController extends AbstractController
{
    /**
     * @Route("/dishes/new", name="createDish")
     */
    public function createDish(Request $request, EntityManagerInterface $doctrine)
    {
        $form = $this->createForm(DishFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dish = $form->getData();
            $dish->setPostDate(new \DateTime());

            $user = $this->getUser();
            $user->addDishCreated($dish);

            $doctrine->persist($dish);
            $doctrine->flush();

            $this->addFlash('success', "Plato {$dish->getName()} creado correctamente.");
        }

        return $this->render(
            'Dish/createDish.html.twig',
            ['dishForm' => $form->createView()]
        );
    }

    /**
     * @Route("/dishes/{id}", name="dishProfile")
     */
    public function showDish($id, EntityManagerInterface $doctrine)
    {
        $repo = $doctrine->getRepository(Dish::class);

        $dish = $repo->find($id);

        $recipes = $dish->getRecipes();

        return $this->render("Dish/dishProfile.html.twig",
            ['dish'=>$dish,
                'recipes' => $recipes
            ]);
    }

    /**
     * @Route("/dishes/delete/{id}", name="deleteDish")
     */
    public function deleteDish(Dish $dish, EntityManagerInterface $doctrine)
    {
        $doctrine->remove($dish);
        $doctrine->flush();
        return $this->redirectToRoute("homePage");
    }
}