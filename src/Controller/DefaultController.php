<?php


namespace App\Controller;

use App\Entity\Dish;
use App\Entity\recipes;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/home", name="homePage")
     */
    public function homePage(EntityManagerInterface $doctrine) : Response
    {
        $repo = $doctrine->getRepository(Dish::class);

        $plate = $repo->findBy(array('type' => 'Plato'));
        $dessert = $repo->findBy(array('type' => 'Postre'));

        return $this->render(
            "home/home.html.twig",
            [
                "plates" => $plate,
                "desserts" => $dessert
            ]
        );
    }

    /**
     * @Route("/user/{id}" , name="profilePage")
     */
    public function profilePage($id, EntityManagerInterface $doctrine)
    {
        $repo = $doctrine->getRepository(User::class);
        $user = $repo->find($id);
        $dishes = $user->getDishCreated();
        $recipes = $user->getRecipesCreated();

        $repo2 = $doctrine->getRepository(recipes::class);
        $recom = $repo2->findBy(array('difficulty' => $user->getExperience()), array('likes' => 'DESC'));

        $like = $user->getRecipesLiked();

        return $this->render("User/UserProfile.html.twig",
            [
                'user' => $user,
                'dishes' => $dishes,
                'recipes' => $recipes,
                'recoms' => $recom,
                'likes' => $like
            ]);
    }
}