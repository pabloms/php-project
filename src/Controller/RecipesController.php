<?php


namespace App\Controller;

use App\Entity\Dish;
use App\Entity\recipes;
use App\Form\DishFormType;
use App\Form\RecipeFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RecipesController extends AbstractController
{
    /**
     * @Route("/dishes/{id}/new-recipe", name="createRecipe")
     */
    public function createRecipe($id, Request $request, EntityManagerInterface $doctrine, Dish $dish)
    {
        $form = $this->createForm(RecipeFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $recipe = $form->getData();
            $recipe->setPostDate(new \DateTime());
            $recipe->setLikes(0);

            $user = $this->getUser();
            $user->addRecipesCreated($recipe);

            $repo = $doctrine->getRepository(Dish::class);
            $dish = $repo->find($id);
            $dish->addRecipe($recipe);

            $doctrine->persist($recipe);
            $doctrine->flush();

            $this->addFlash('success', "Receta creado correctamente.");
        }

        return $this->render(
            'Recipe/createRecipe.html.twig',
            ['recipeForm' => $form->createView(),
                'dish' => $dish
            ]
        );
    }

    /**
     * @Route("/recipes/{id}/addlike", name="addLike")
     */
    public function addLike($id, EntityManagerInterface $doctrine)
    {
        $user = $this->getUser();

        $repo = $doctrine->getRepository(recipes::class);
        $recipe = $repo->find($id);

        $likes = $recipe->getLikes();
        $recipe->setLikes($likes + 1);

        $user->addRecipesLiked($recipe);

        $doctrine->persist($recipe);
        $doctrine->persist($user);
        $doctrine->flush();

        return $this->redirectToRoute("homePage");
    }
}