<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"username"}, message="There is already an account with this username")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity=Dish::class, mappedBy="user")
     */
    private $dishCreated;

    /**
     * @ORM\OneToMany(targetEntity=recipes::class, mappedBy="user")
     */
    private $recipesCreated;

    /**
     * @ORM\ManyToMany(targetEntity=recipes::class, inversedBy="likedUsers")
     */
    private $recipesLiked;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $experience;

    public function __construct()
    {
        $this->dishCreated = new ArrayCollection();
        $this->recipesCreated = new ArrayCollection();
        $this->recipesLiked = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Dish[]
     */
    public function getDishCreated(): Collection
    {
        return $this->dishCreated;
    }

    public function addDishCreated(Dish $dishCreated): self
    {
        if (!$this->dishCreated->contains($dishCreated)) {
            $this->dishCreated[] = $dishCreated;
            $dishCreated->setUser($this);
        }

        return $this;
    }

    public function removeDishCreated(Dish $dishCreated): self
    {
        if ($this->dishCreated->removeElement($dishCreated)) {
            // set the owning side to null (unless already changed)
            if ($dishCreated->getUser() === $this) {
                $dishCreated->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|recipes[]
     */
    public function getRecipesCreated(): Collection
    {
        return $this->recipesCreated;
    }

    public function addRecipesCreated(recipes $recipesCreated): self
    {
        if (!$this->recipesCreated->contains($recipesCreated)) {
            $this->recipesCreated[] = $recipesCreated;
            $recipesCreated->setUser($this);
        }

        return $this;
    }

    public function removeRecipesCreated(recipes $recipesCreated): self
    {
        if ($this->recipesCreated->removeElement($recipesCreated)) {
            // set the owning side to null (unless already changed)
            if ($recipesCreated->getUser() === $this) {
                $recipesCreated->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|recipes[]
     */
    public function getRecipesLiked(): Collection
    {
        return $this->recipesLiked;
    }

    public function addRecipesLiked(recipes $recipesLiked): self
    {
        if (!$this->recipesLiked->contains($recipesLiked)) {
            $this->recipesLiked[] = $recipesLiked;
        }

        return $this;
    }

    public function removeRecipesLiked(recipes $recipesLiked): self
    {
        $this->recipesLiked->removeElement($recipesLiked);

        return $this;
    }

    public function getExperience(): ?string
    {
        return $this->experience;
    }

    public function setExperience(string $experience): self
    {
        $this->experience = $experience;

        return $this;
    }
}
